//
//  InviteServices.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 21/06/17.
//
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class InviteServices: NSObject {
    func sendInvitation(_ invitation : UserInvite, handler : @escaping (Bool, String?) -> Void) {
        let url : String = Constants.friendInvitation
        
        Alamofire.request(url, method: .post, parameters: invitation.toJSON()).validate().responseObject { (response : DataResponse<InviteResponse>) in
            switch response.result {
            case .success:
                if let responseData = response.result.value {
                    if let error = responseData.returnError, let errorMessage = error.errorMessage {
                        handler(false, errorMessage)
                    } else {
                        handler(true, responseData.success)
                    }
                } else {
                    handler(false, "An unexpected error occurred when try sent friend invitation")
                }
                
            case .failure(let error):
                handler(false, error.localizedDescription)
            }
        }
    }
}
