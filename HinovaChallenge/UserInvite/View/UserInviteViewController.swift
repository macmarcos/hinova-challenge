//
//  UserInviteViewController.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 20/06/17.
//
//

import UIKit

class UserInviteViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var scrollContent : UIScrollView!
    @IBOutlet weak var associateNameField: UITextField!
    @IBOutlet weak var associateMailField: UITextField!
    @IBOutlet weak var associatePhoneField: UITextField!
    @IBOutlet weak var friendNameField: UITextField!
    @IBOutlet weak var friendMailField: UITextField!
    @IBOutlet weak var friendPhoneField: UITextField!
    
    private var currentField : UITextField? = nil
    private var mask : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Invite Friend"
        
        initUI()
    }
    
    //MARK: - UI
    func initUI() {
        addButtons()
        configureFields()
        configureGestures()
    }
    
    func addButtons() {
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(closeScreen))
        let sendInvitation = UIBarButtonItem(title: "Invite", style: .plain, target: self, action: #selector(inviteFriend))
        
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.rightBarButtonItem = sendInvitation
    }
    
    func configureFields() {
        for view in scrollContent.subviews {
            if view is UITextField {
                let textField = view as! UITextField
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 5))
                
                textField.delegate = self
                textField.leftView = paddingView
                textField.leftViewMode = .always
                
                // Configure borders
                textField.layer.cornerRadius = 5
                textField.layer.masksToBounds = true
            }
        }
    }
    
    func configureGestures() {
        let scrollTap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        
        scrollContent.addGestureRecognizer(scrollTap)
    }
    
    //MARK: - Actions
    func closeScreen() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func hideKeyboard() {
        scrollContent.endEditing(true)
    }

    func inviteFriend() {
        self.view.endEditing(true)
        
        if !validateFields() {
            return
        }
        
        MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: true)
        
        let invite = prepareData()
        
        InviteServices().sendInvitation(invite) { (success, message) in
            MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: true)
            
            if !success {
                Utils.showMessage(message!, view: self)
            } else {
                Utils.showMessage(message!, view: self, handler: { (action) in
                    self.closeScreen()
                })
            }
        }
    }
    
    func prepareData() -> UserInvite {
        let userInvite = UserInvite()
        let invite = Invites()
        
        invite.associateName = associateNameField.text
        invite.associateEmail = associateMailField.text
        invite.associatePhone = associatePhoneField.text
        invite.friendName = friendNameField.text
        invite.friendEmail = friendMailField.text
        invite.friendPhone = friendPhoneField.text
        invite.createDate = Utils.formatDate(Date(), pattern: "yyyy-MM-dd")
        
        userInvite.indication = invite
        userInvite.to = "romulo.marques@hinovamobile.com.br"
        
        return userInvite
    }
    
    func validateFields() -> Bool {
        var message = ""
        
        if (associateNameField.text?.isEmpty)! {
            message = "Your name is required"
        } else if (associateMailField.text?.isEmpty)! {
            message = "Your e-mail is required"
        } else if !Utils.validateEmail(associateMailField.text!) {
            message = "Your e-mail is invalid"
        } else if (associatePhoneField.text?.isEmpty)! {
            message = "Your phone is required"
        } else if (friendNameField.text?.isEmpty)! {
            message = "Friend name is required"
        } else if (friendMailField.text?.isEmpty)! {
            message = "Friend e-mail is required"
        } else if !Utils.validateEmail(friendMailField.text!) {
            message = "Friend e-mail is invalid"
        } else if (friendPhoneField.text?.isEmpty)! {
            message = "Friend phone is required"
        }
        
        if !message.isEmpty {
            Utils.showMessage(message, view: self)
            
            return false
        }
        
        return true
    }
    
    func previousAction() {
        if currentField == associatePhoneField {
            associateMailField.becomeFirstResponder()
        } else {
            friendMailField.becomeFirstResponder()
        }
    }
    
    func nextAction() {
        if currentField == associatePhoneField {
            friendNameField.becomeFirstResponder()
        } else {
            currentField?.endEditing(true)
        }
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == associateNameField {
            associateMailField.becomeFirstResponder()
        } else if textField == associateMailField {
            associatePhoneField.becomeFirstResponder()
        } else if textField == associatePhoneField {
            friendNameField.becomeFirstResponder()
        } else if textField == friendNameField {
            friendMailField.becomeFirstResponder()
        } else if textField == friendMailField {
            friendPhoneField.becomeFirstResponder()
        } else {
            textField.endEditing(true)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentField = textField
        
        if textField == associatePhoneField || textField == friendPhoneField {
            mask = "(99) 99999-9999"
            
            let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50))
            
            numberToolbar.barStyle = .black
            numberToolbar.tintColor = UIColor.white
            numberToolbar.items = createToolbarItems()
            
            numberToolbar.sizeToFit()
            
            textField.inputAccessoryView = numberToolbar
        } else {
            mask = ""
            textField.inputAccessoryView = nil
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.inputAccessoryView = nil
    }
    
    func createToolbarItems() -> [UIBarButtonItem] {
        let previousButton = UIBarButtonItem(title: "Previous", style: .plain, target: self, action: #selector(previousAction))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let nextButton = UIBarButtonItem(title: currentField == associatePhoneField ? "Next" : "Done", style: .plain, target: self, action: #selector(nextAction))
        
        let items = [previousButton, flexibleSpace, nextButton]
        
        return items
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if mask.isEmpty {
            return true
        } else if textField.text!.characters.count == mask.characters.count {
            if (string == "") == false {
                return false
            } else {
                return true
            }
        } else {
            if (string == "") == false {
                self.formatInput(textField, string: string, range: range)
                return false
            }
            
            return true
        }
    }
    
    func formatInput(_ textField : UITextField, string: String, range : NSRange) {
        let value = textField.text
        
        var formattedValue = value
        var range = range
        
        range.length = 1
        
        var _mask = (mask as NSString).substring(with: range)
        
        let regex = "[0-9]*"
        let regexTest = NSPredicate(format: "SELF MATCHES %@", regex)
        
        if !regexTest.evaluate(with: _mask) {
            formattedValue = formattedValue?.appending(_mask)
        }
        
        if range.location + 1 < mask.characters.count {
            var newRange = NSRange()
            newRange.location = range.location + 1
            newRange.length = 1
            
            _mask = (mask as NSString).substring(with: newRange)
            
            if _mask == " " {
                formattedValue = formattedValue?.appending(_mask)
            }
        }
        
        formattedValue = formattedValue?.appending(string)
        textField.text = formattedValue
    }
    
}
