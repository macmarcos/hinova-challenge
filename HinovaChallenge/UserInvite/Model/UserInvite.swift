//
//  UserInvite.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 20/06/17.
//
//

import UIKit
import ObjectMapper

class UserInvite: Mappable {
    var indication : Invites?
    var to : String?
    var copies : [Any?]? = [Any?]()
    
    init() {
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        indication <- map["Indicacao"]
        to <- map["Remetente"]
        copies <- map["Copias"]
    }

}
