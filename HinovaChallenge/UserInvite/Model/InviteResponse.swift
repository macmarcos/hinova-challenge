//
//  InviteResponse.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 20/06/17.
//
//

import UIKit
import ObjectMapper

class InviteResponse: BaseModel {
    var success : String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        success <- map["Sucesso"]
    }
}
