//
//  Invites.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 20/06/17.
//
//

import UIKit
import ObjectMapper

class Invites: Mappable {
    var associationCode : String? = "601"
    var associateCPF : String? = "123123"
    var vechile : String? = "asd"
    var createDate : String?
    var associateEmail : String?
    var associateName : String?
    var associatePhone : String?
    var friendName : String?
    var friendPhone : String?
    var friendEmail : String?
    
    init() {
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        associationCode <- map["CodigoAssociacao"]
        associateCPF <- map["CpfAssociado"]
        vechile <- map["PlacaVeiculoAssociado"]
        createDate <- map["DataCriacao"]
        associateEmail <- map["EmailAssociado"]
        associateName <- map["NomeAssociado"]
        associatePhone <- map["TelefoneAssociado"]
        friendName <- map["NomeAmigo"]
        friendPhone <- map["TelefoneAmigo"]
        friendEmail <- map["EmailAmigo"]
    }

}
