//
//  Constants.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 19/06/17.
//
//

import UIKit

class Constants: NSObject {
    fileprivate static let kBaseURL = "http://app.hinovamobile.com.br/ProvaConhecimentoWebApi"
    
    static let officesList = String(format: "%@/%@", kBaseURL, "/api/Oficina?codigoAssociacao=601")
    static let friendInvitation = String(format: "%@/%@", kBaseURL, "/api/Indicacao")
}
