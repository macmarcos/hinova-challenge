//
//  ReturnErrorModel.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 20/06/17.
//
//

import ObjectMapper

class ReturnErrorModel : Mappable {
    var errorMessage : String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        errorMessage <- map["retornoErro"]
    }
    
}

