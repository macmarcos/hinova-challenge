//
//  BaseModel.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 20/06/17.
//
//

import ObjectMapper

class BaseModel : Mappable {
    var returnError : ReturnErrorModel?
    var token : Any?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        returnError <- map["RetornoErro"]
        token <- map["Token"]
    }
    
}


