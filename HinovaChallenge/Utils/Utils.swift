//
//  Utils.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 19/06/17.
//
//

import UIKit

class Utils: NSObject {
    static func showMessage(_ message : String, view : UIViewController, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: "HINOVA", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: handler)
        
        alert.addAction(action)
        
        view.present(alert, animated: true, completion: nil)
    }
    
    static func convertBase64ToImage(_ imageData : String?) -> UIImage {
        if let decodedData = Data(base64Encoded: imageData!, options: .ignoreUnknownCharacters) {
            if let image = UIImage(data: decodedData) {
                return image
            }
    
            return UIImage()
        }
        
        return UIImage()
    }
    
    static func formatDate(_ date : Date, pattern : String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = pattern
        
        return formatter.string(from: date)
    }
    
    static func validateEmail(_ email : String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: email)
    }
}
