//
//  OfficesResponse.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 20/06/17.
//
//

import ObjectMapper

class OfficesResponse : BaseModel {
    var listOffices : [Offices]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        listOffices <- map["ListaOficinas"]
    }
    
}

