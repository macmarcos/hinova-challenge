//
//  Offices.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 19/06/17.
//
//

import UIKit
import ObjectMapper

class Offices: Mappable {
    var id : Int?
    var name : String?
    var description : String?
    var shortDescription : String?
    var address : String?
    var latitude : String?
    var longitude : String?
    var photo : String?
    var userRate : Int?
    var associationCode : Int?
    var email : String?
    var phone_1 : String?
    var phone_2 : String?
    var active : Bool?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["Id"]
        name <- map["Nome"]
        description <- map["Descricao"]
        shortDescription <- map["DescricaoCurta"]
        address <- map["Endereco"]
        latitude <- map["Latitude"]
        longitude <- map["Longitude"]
        photo <- map["Foto"]
        userRate <- map["AvaliacaoUsuario"]
        associationCode <- map["CodigoAssociacao"]
        email <- map["Email"]
        phone_1 <- map["Telefone1"]
        phone_2 <- map["Telefone2"]
        active <- map["Ativo"]
    }
    
}
