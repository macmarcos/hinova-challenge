//
//  OfficesService.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 19/06/17.
//
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class OfficesService: NSObject {
    func listOffices(_ handler : @escaping ([Offices]?, String?) -> Void) {
        let url : String = Constants.officesList
        
        Alamofire.request(url).validate().responseObject { (response : DataResponse<OfficesResponse>) in
            switch response.result {
            case .success:
                if let responseData = response.result.value{
                    if let error = responseData.returnError, let errorMessage = error.errorMessage {
                        handler(nil, errorMessage)
                    } else {
                        handler(responseData.listOffices, nil)
                    }
                } else {
                    handler(nil, "An unexpected error occurred when try loading offices")
                }
                
            case .failure(let error):
                handler(nil, error.localizedDescription)
            }
        }
    }
}
