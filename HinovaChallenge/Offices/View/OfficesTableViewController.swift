//
//  OfficesTableViewController.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 19/06/17.
//
//

import UIKit

class OfficesTableViewController: UITableViewController {
    var items : [Offices] = [Offices]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        
        // Customize table view
        configureTableView()
        
        loadOffices()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Offices"
    }
    
    func loadOffices() {
        MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: true)
        
        OfficesService().listOffices { (offices, error) in
            MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: true)
            
            if let errorMessage = error {
                Utils.showMessage(errorMessage, view: self)
                
                return
            }
            
            if let officesList = offices {
                if officesList.count == 0 {
                    Utils.showMessage("Offices List Empty", view: self)
                    
                    return
                }
                
                self.items = officesList
                self.tableView.reloadData()
            }
        }
    }
    
    func configureTableView() {
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        
        self.tableView.register(UINib(nibName: "OfficesTableViewCell", bundle: nil), forCellReuseIdentifier: "OfficesCell")
    }

    // MARK: - UITableViewDelegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OfficesCell", for: indexPath) as? OfficesTableViewCell else {
            return UITableViewCell()
        }

        let office = items[indexPath.row]
        
        cell.initWithOffice(office)
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 97
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let officeDetailController = OfficeDetailViewController()
        let office = items[indexPath.row]
        
        officeDetailController.office = office
        
        self.navigationController?.pushViewController(officeDetailController, animated: true)
    }
    
    // MARK: Actions
    @IBAction func inviteFriendButtonClick() {
        let userInviteController = UserInviteViewController()
        let rootController = UINavigationController(rootViewController: userInviteController)
        
        self.navigationController?.present(rootController, animated: true, completion: nil)
    }
    
}
