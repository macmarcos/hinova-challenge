//
//  OfficesTableViewCell.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 19/06/17.
//
//

import UIKit

class OfficesTableViewCell: UITableViewCell {
    @IBOutlet weak var officePhoto : UIImageView!
    @IBOutlet weak var officeName : UILabel!
    @IBOutlet weak var progress : UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        officePhoto.layer.borderColor = UIColor.lightGray.cgColor
        officePhoto.layer.borderWidth = 1.5
        officePhoto.layer.masksToBounds = true
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        progress.isHidden = false
        progress.startAnimating()
        
        officePhoto.backgroundColor = UIColor.lightGray
        officePhoto.image = nil
        
        officeName.text = ""
    }
    
    func initWithOffice(_ office : Offices) {
        // Photo
        DispatchQueue.main.async {
            self.officePhoto.image = Utils.convertBase64ToImage(office.photo)
            self.progress.stopAnimating()
        }
        
        // Name
        officeName.text = office.name
    }

}
