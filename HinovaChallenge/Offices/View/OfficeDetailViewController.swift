//
//  OfficeDetailViewController.swift
//  HinovaChallenge
//
//  Created by Marcos Lacerda on 20/06/17.
//
//

import UIKit
import MapKit

class OfficeDetailViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet weak var officePhoto: UIImageView!
    @IBOutlet weak var progress : UIActivityIndicatorView!
    @IBOutlet weak var officeShortDescription: UILabel!
    @IBOutlet weak var officeDescription: UILabel!
    @IBOutlet weak var officeAddress: UILabel!
    @IBOutlet weak var officeMail: UILabel!
    @IBOutlet weak var officePhones: UILabel!
    @IBOutlet weak var officeLocationMap: MKMapView!
    
    var office : Offices?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = office!.name
        self.navigationController?.navigationBar.topItem?.title = ""
        
        populateData()
    }
    
    //MARK : - UI
    let regionRadius: CLLocationDistance = 500
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        
        officeLocationMap.setRegion(coordinateRegion, animated: true)
    }
    
    //MARK: - Actions
    func populateData() {
        // Photo
        DispatchQueue.main.async {
            self.officePhoto.image = Utils.convertBase64ToImage(self.office!.photo)
            self.progress.stopAnimating()
        }
        
        officeShortDescription.text = office!.shortDescription
        
        // Parsing line breaks
        let utf8str = office!.description?.data(using: String.Encoding.utf8)
        
        if let base64Encoded = utf8str?.base64EncodedString() {
            let decodedData = NSData(base64Encoded: base64Encoded, options: []),
            decodedString = NSString(data: decodedData! as Data, encoding: String.Encoding.utf8.rawValue)
            
            officeDescription.text = (decodedString! as String).replacingOccurrences(of: "\\n", with: "\n")
        }
        
        officeAddress.text = String(format : "Address: %@", office!.address ?? "Address not avaliable")
        officeMail.text = String(format: "E-mail: %@", office?.email ?? "E-mail not avaliable")
        
        let phoneOne = office!.phone_1 ?? ""
        let phoneTwo = office!.phone_2 ?? ""
        
        if phoneOne.isEmpty && phoneTwo.isEmpty {
            officePhones.text = "Phones not avaliable"
        } else if phoneTwo.isEmpty {
            officePhones.text = String(format: "Phone(s): %@", phoneOne)
        } else {
            officePhones.text = String(format: "Phones: %@ / %@", phoneOne, phoneTwo)
        }
        
        // Check if location avaliable
        if let latitudeString = office!.latitude, let longitudeString = office!.longitude {
            let latitude = Double(latitudeString)
            let longitude = Double(longitudeString)
            
            if latitude != 0 && longitude != 0 {
                addMapLocation(CLLocation(latitude: CLLocationDegrees(latitude!), longitude: CLLocationDegrees(longitude!)))
            }
        }
        
        // Adjustment Sizes
        officeDescription.sizeToFit()
        officeAddress.sizeToFit()
    }
    
    private func addMapLocation(_ location : CLLocation) {
        let annotation = MKPointAnnotation()
        
        annotation.title = office!.name
        annotation.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        officeLocationMap.addAnnotation(annotation)
        
        centerMapOnLocation(location: location)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var view: MKPinAnnotationView
        
        view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        view.canShowCallout = true
        view.calloutOffset = CGPoint(x: -5, y: 5)
        view.rightCalloutAccessoryView = UIButton(type: UIButtonType.detailDisclosure) as UIView
        
        return view
    }
}
